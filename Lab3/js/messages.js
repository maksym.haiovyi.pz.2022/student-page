class MessagesWindow {

    constructor() {
        this.button = $("#openNotificationsButton");    
        this.messagesWindow = $("#notificationWindow");

        this.button.on("click", this.DisplayNotifications.bind(this));
    }

    DisplayNotifications() {
        if (this.messagesWindow.hasClass("unvisible")) {
            this.messagesWindow.addClass("visible").removeClass("unvisible");
            setTimeout(() => {
                this.messagesWindow.css('opacity', "1");
            }, 50);
        } else {
            this.messagesWindow.css('opacity', "0");
            setTimeout(() => {
                this.messagesWindow.addClass("unvisible").removeClass("visible");
            }, 400);
        }
    }

}