class Student {

    constructor(group, firstname, lastname, gender, birthday) {
        this.group = group;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.birthday = birthday;
    }

    GetFullName() {
        return this.firstname + " " + this.lastname;
    }
}