class Warning {

    constructor() {
        this.warningContainer = $("#warningContainer");
        this.warningBlock = $("#warningContainer .warning-block");
        this.warningText = $("#warningText");

        this.okButton = $("#confirmWarningButton");
        this.cancelButton = $("#cancelWarningButton");
        this.closeButton = $("#closeWarningButton");
    }

    ShowMessage(message) {
        return new Promise((resolve, reject) => {
            this.warningText.text(message);
            this.warningContainer.removeClass("closed");
            this.warningContainer.addClass("opened");
            setTimeout(() => {
                this.warningContainer.css('opacity', '1');
                this.warningBlock.css('transform', 'translateY(50px)');
            }, 100);

            this.okButton.off("click").on("click", () => {
                this.HideMessage();
                resolve(true);
            });

            this.cancelButton.off("click").on("click", () => {
                this.HideMessage();
                resolve(false);
            });

            this.closeButton.off("click").on("click", () => {
                this.HideMessage();
                resolve(false);
            });
        });
    }

    HideMessage() {
        this.warningContainer.css('opacity', '0');
        this.warningBlock.css('transform', 'translateY(40px)');
        setTimeout(() => {
            this.warningContainer.removeClass("opened");
            this.warningContainer.addClass("closed");
        }, 400);
    }
}