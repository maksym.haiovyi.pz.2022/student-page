$(document).ready(function() {
    const messages = new MessagesWindow();
    const userProfile = new UserProfileWindow();
    const form = new Form();

    let studentsTable = $("#studentsTable");
    let openAddFormButton = $("#openFormButton");
    let closeFormButton = $("#closeFormButton");

    // Event handlers
    openAddFormButton.on("click", () => {
        form.OpenAddForm("Add student");
    });

    closeFormButton.on("click", (event) => {
        event.preventDefault();
        form.CloseForm();
    });

    studentsTable.on("click", ".edit-student-button", function() {
        let buttonContext = this;
        form.OpenEditForm("Edit student", buttonContext);
    });

    studentsTable.on("click", ".delete-student-button", function() {
        let buttonContext = this;
        form.OpenRemoveMessage(buttonContext);
    });

    studentsTable.on("change", ".choose--student__checkbox", function() {
        let checkboxContext = this;
        form.SelectStudent(checkboxContext);
    });
});