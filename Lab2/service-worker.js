// self.addEventListener('install', function(event) {
//     event.waitUntil(
//         caches.open('CMS_Lab_cache').then(function(cache) {
//         return cache.addAll([
//             'css/bootstrap.min.css',
//             'css/media.css',
//             'css/style.css',
//             'img/user.svg',
//             'img/bell.svg',
//             'img/pen.svg',
//             'img/plus.svg',
//             'img/xmark.svg',
//             'img/bars.svg',
//             'js/',
//             'index.html'
//         ]);
//         })
//     );
// });

// self.addEventListener('fetch', (event) => {
//     event.respondWith(
//       caches.open('CMS_Lab_cache').then((cache) => {
//         return cache.match(event.request.url).then((cachedResponse) => {
//           if (cachedResponse) {
//             console.log(cachedResponse, "find in cache");
//             return cachedResponse;
//           }
  
//           return fetch(event.request).then((fetchedResponse) => {    
//             cache.put(event.request, fetchedResponse.clone());
//             console.log("not find in cache", fetchedResponse);
//             return fetchedResponse;
//           }).catch(() => {
//             // Повертаємо стандартну сторінку або повідомлення про відсутність підключення
//             return new Response('Offline mode: Unable to fetch the resource.');
//           });
//         });
//       })
//     );
//   });
  