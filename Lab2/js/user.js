class UserProfileWindow {

    constructor() {
        this.button = $("#openUserProfileButton");
        this.userProfileWindow = $("#userProfileWindow");

        this.button.on("click", this.DisplayUserProfile.bind(this));
    }

    DisplayUserProfile() {
        if(this.userProfileWindow.hasClass("visible")) {
            this.userProfileWindow.css('opacity', '0');
            setTimeout(() => {        
                this.userProfileWindow.addClass("unvisible").removeClass("visible");
            }, 400);
        } else {
            this.userProfileWindow.addClass("visible").removeClass("unvisible");
            setTimeout(() => {
                this.userProfileWindow.css('opacity', '1');
            }, 50);
        }
    }

}