class Form {

    constructor() {
        this.id = 1;
        this.form = $("#form");
        this.formTitle = $("#formTitle");
        this.createButton = $("#createStudentButton");
        this.confirmChangesButton = $("#confirmChangesButton");
        this.closeButton = $("#closeFormButton");

        this.groupField = $("#studentGroup");
        this.firstnameField = $("#studentName");
        this.lastnameField = $("#studentLastname");
        this.genderField = $("#studentGender");
        this.birthday = $("#studentDate");

        this.currentRow = undefined;

        this.createButton.on("click", (event) => {
            event.preventDefault();
            this.AddStudent();
        });

        this.confirmChangesButton.on("click", (event) => {
            event.preventDefault();
            this.EditStudent();
        })
        
    }

    OpenAddForm(message) {
        this.formTitle.text(message);

        this.form.removeClass("form-unvisible");
        setTimeout(() => {
            this.form.css('opacity', '1');
        }, 20);
    }

    AddStudent() {
        let student = new Student(this.groupField.val(), 
                                  this.firstnameField.val(), 
                                  this.lastnameField.val(), 
                                  this.genderField.val(), 
                                  this.birthday.val());

        let newRow = $('<tr class="d-flex">').append(
            $('<td class="content-center">').html('<input type="checkbox" class="choose--student__checkbox">'),
            $('<td class="content-center">').text(this.id),
            $('<td class="content-center">').text(student.group),
            $('<td class="content-center">').text(student.GetFullName()),
            $('<td class="content-center">').text(student.gender),
            $('<td class="content-center">').text(student.birthday),
            $('<td class="content-center">').html('<div class="choose-student-indicator unselected rounded-circle">'),
            $('<td class="d-flex content-center">').html(
                '<button class="edit-student-button general-button smooth-object rounded border">' +
                    '<img class="base-icon" src="img/pen.svg" alt="Edit">' +
                '</button>' +
                '<button class="delete-student-button general-button smooth-object rounded border">' +
                    '<img class="base-icon" src="img/xmark.svg" alt="Delete">' +
                '</button>'
            )   
        );

        this.DoGETRequest(student);

        this.id++;

        $("#studentsTable tbody").append(newRow);

        this.CloseForm();
    }

    DoGETRequest(student) {
        $.ajax({
            type: "GET",
            url: 'http://127.0.0.1:5500/',
            data: {
                group: student.group,
                firstname: student.firstname,
                lastname: student.lastname,
                gender: student.gender,
                birthday: student.birthday
            },
            success: function(response) {
                console.log("Success", response);
                let params = $.param({
                    group: student.group,
                    firstname: student.firstname,
                    lastname: student.lastname,
                    gender: student.gender,
                    birthday: student.birthday
                });
                window.history.pushState({}, '', window.location.pathname + '?' + params);
            },
            error: function(error) {
                console.error("Error:", error, "Can't save user:", student);
            }
        });
    }

    OpenEditForm(message, buttonContext) {
        this.formTitle.text(message);

        this.form.removeClass("form-unvisible");
        setTimeout(() => {
            this.form.css('opacity', '1');
        }, 20);

        this.currentRow = $(buttonContext).closest("tr");
        let studentName = this.currentRow.find("td:eq(3)").text().split(" ");  

        this.groupField.val(this.currentRow.find("td:eq(2)").text());
        this.firstnameField.val(studentName[0]);
        this.lastnameField.val(studentName[1]);
        this.genderField.val(this.currentRow.find("td:eq(4)").text());
        this.birthday.val(this.currentRow.find("td:eq(5)").text());
    }

    EditStudent() {
        if (this.currentRow == undefined) {
            let warning = new Warning();
            warning.ShowMessage("You have not selected a student to edit.");
            return;
        }

        this.currentRow.find("td:eq(2)").text(this.groupField.val());
        this.currentRow.find("td:eq(3)").text(this.firstnameField.val() + " " + this.lastnameField.val());
        this.currentRow.find("td:eq(4)").text(this.genderField.val());
        this.currentRow.find("td:eq(5)").text(this.birthday.val());

        this.CloseForm();
    }

    SelectStudent(checkboxContext) {
        let indicator = $(checkboxContext).closest("tr").find(".choose-student-indicator");
        if (indicator.hasClass("unselected")) {
            indicator.removeClass("unselected").addClass("selected");
        } else {
            indicator.removeClass("selected").addClass("unselected");
        }
    }

    OpenRemoveMessage(buttonContext) {
        this.currentRow = $(buttonContext).closest("tr");
        let warning = new Warning();
        let text = "Are you sure you want to delete " + this.currentRow.find("td:eq(3)").text() + "?";
        warning.ShowMessage(text).then(result => {
            if (result)
                this.currentRow.remove();
        });
    }

    CloseForm() {
        this.form.css('opacity', '0');
        setTimeout(() => {
            this.form.addClass("form-unvisible");
        }, 400);
        this.ClearFields();
    }
    
    ClearFields() {
        this.groupField.val("PZ-21");
        this.firstnameField.val("");
        this.lastnameField.val("");
        this.genderField.val("M");
        this.birthday.val("");
        this.currentRow = undefined;
    }
}