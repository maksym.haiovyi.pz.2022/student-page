window.addEventListener("load", function (event) {
    AddStudent();
    OpenMessages();
    OpenUserProfile();
});

// Блок повідомлень
function OpenMessages() {
    let messagesButton = document.getElementById("messages-button");
    let messages = document.getElementById('messages');

    messagesButton.addEventListener('click', DisplayMessagesBlock);
    document.addEventListener('click', ClickedOutsideMessagesBlock);

    // Відкриття блоку при натиску на дзвоник
    function DisplayMessagesBlock() {
        if (!messages.classList.contains('active')) {
            messages.classList.add('active');

            setTimeout(function() {
                messages.style.opacity = "1";
                messages.style.transform = "translateY(0)";
            }, 100);
        } else {
            messages.style.opacity = "0";
            messages.style.transform = "translateY(15px)";

            setTimeout(function() {
                messages.classList.remove('active');
            }, 400);
            
        }    
    }

    // Дії, якщо натиснуто за межами блоку повідомлень
    function ClickedOutsideMessagesBlock(event) {
        const isClickInside = messages.contains(event.target) || messagesButton.contains(event.target);
        if (!isClickInside && messages.classList.contains('active')) {
            DisplayMessagesBlock();
        }
    }
}

// Профіль користувача
function OpenUserProfile() {
   
    let userProfileButton = document.getElementById("userProfileButton");

    userProfileButton.addEventListener('click', DisplayUserBlock);
    document.addEventListener('click', ClickedOutsideProfileBlock);

    function DisplayUserBlock() {
        let userProfileBlock = document.getElementById("userProfileBlock");

        if (!userProfileBlock.classList.contains('active')) {
            userProfileBlock.classList.add('active');

            setTimeout(function() {
                userProfileBlock.style.opacity = "1";
                userProfileBlock.style.transform = "translateY(0)";
            }, 100);
        } else {   
            userProfileBlock.style.opacity = "0";
            userProfileBlock.style.transform = "translateY(15px)";

            setTimeout(function() {
                userProfileBlock.classList.remove('active');
            }, 400);
        }
    }

    function ClickedOutsideProfileBlock(event) {
        const isClickInside = userProfileBlock.contains(event.target) || userProfileButton.contains(event.target);
        if (!isClickInside && userProfileBlock.classList.contains('active')) {
            DisplayUserBlock();
        }
    }
}

/*
    Форма додавання
*/

function AddStudent() {
    // Таблиця студентів
    let table = document.getElementById('studentsTable');

    let addFormContainer = document.getElementById("addFormContainer");

    let addStudentButton = document.getElementById("addStudentButton");
    let closeAddFormButton = document.getElementById("closeAddFormButton");
    let okButton = document.getElementById("addStudentOkButton");

    // Закрити форму
    // При натиску на хрестикf
    // При натиску на "ok"

    closeAddFormButton.addEventListener("click", CloseAddForm);
    okButton.addEventListener("click", CloseAddForm);

    // Відкрити форму
    addStudentButton.addEventListener("click", OpenAddForm);

    // Додати студента до таблиці
    let addStudentForm = document.getElementById('AddStudentForm');
    let addConfirmFormButton = document.getElementById('addConfirmFormButton');

    addConfirmFormButton.addEventListener('click', function() {

        let group = document.getElementById("addGroupSelect").value;
        let firstName = document.getElementById("addFirstName").value;
        let lastName = document.getElementById("addLastName").value;
        let gender = document.getElementById("addGender").value;
        let birthday = document.getElementById("addBirthday").value;

        let newRow = document.createElement('tr');
        newRow.innerHTML = `
            <td><input type="checkbox" aria-label="Select" class="indicator-checkbox"></td>
            <td>${group}</td>
            <td>${firstName} ${lastName}</td>
            <td>${gender}</td>
            <td>${birthday}</td>
            <td>
                <div class="indicator indicator-unchecked"></div>
            </td>
            <td>
                <button class="edit-student" aria-label="Edit student"></button>
                <button class="delete-student" aria-label="Delete student"></button>
            </td>
        `;
        table.appendChild(newRow);

        /*
            Встановлення прослуховування кожного рядка таблиці
            Видалення студента з таблиці (окремо кожного)
        */
        DeleteStudent();

        // Зміна елементів при обиранні студента
        SelectStudent();

        /*
            Редагування студентів
        */
        EditStudent();

        // Clear form inputs
        addStudentForm.reset();

        alert("The student was successfully added!");
    }); 
}

function CloseAddForm() {
    addFormContainer.style.opacity = "0";
    addFormContainer.querySelector(".form").style.transform = "translateY(-10px)";
    setTimeout(function(){
        addFormContainer.style.display = "none";
    }, 400);
}

function OpenAddForm() {
    addFormContainer.style.display = "grid";

    setTimeout(function(){
        addFormContainer.style.opacity = "1";
        addFormContainer.querySelector(".form").style.transform = "translateY(10px)";
    }, 100);
}

/* Видалення студента з таблиці (окремо кожного) */
function DeleteStudent() {
    let cancelStudentButtons = document.querySelectorAll(".delete-student");
    cancelStudentButtons.forEach(button => {
        button.addEventListener('click', function() {
            let row = this.closest('tr');
            let fullName = row.cells[2].textContent;
            if (confirm("Are you sure you want to delete row " + fullName + "?")) {
                this.closest('tr').remove();
            }
        });
    });
};

/* Редагування студента */
function EditStudent() {
    let editStudentsButton = document.querySelectorAll(".edit-student");
    let editStudentForm = document.getElementById("EditFormContainer");
    let closeEditFormButton = document.getElementById('closeEditFormButton');

    // Введення поточних даних у форму
    editStudentsButton.forEach(button => {
        button.addEventListener('click', function() {
            
            // Відкрити форму редагування
            OpenEditForm(editStudentForm);
            
            // Закрити форму редагування
            closeEditFormButton.addEventListener('click', function() {
                CloseEditForm(editStudentForm);
            });
        });
    });
}

// Відкриття форми редагування
function OpenEditForm(editStudentForm) {
    editStudentForm.style.display = "grid";

    setTimeout(function() {
        editStudentForm.style.opacity = "1";
        editStudentForm.querySelector(".form").style.transform = "translateY(10px)";
    }, 100);
}

// Закриття форми редагування
function CloseEditForm(editStudentForm) {
    editStudentForm.style.opacity = "0";
    editStudentForm.querySelector(".form").style.transform = "translateY(-10px)";
    setTimeout(function(){
        editStudentForm.style.display = "none";
    }, 400);
}

// Запалення індикатора
function SelectStudent() {
    // Індикатор
    // Вибираємо всі чекбокси у таблиці
    const checkboxes = document.querySelectorAll('input[class="indicator-checkbox"]');
    checkboxes.forEach(checkbox => {
        // Додаємо слухача подій для зміни класу елемента "status-indicator"
        checkbox.addEventListener('change', function() {
            const statusIndicator = this.closest('tr').querySelector('.indicator');
            if (this.checked) {
                statusIndicator.classList.add('indicator-checked');
                statusIndicator.classList.remove('indicator-unchecked');
            } else {
                statusIndicator.classList.add('indicator-unchecked');
                statusIndicator.classList.remove('indicator-checked');
            }
        });
    });
}