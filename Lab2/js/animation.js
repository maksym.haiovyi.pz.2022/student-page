$(document).ready(function () {
    UserItemsHover();
    BaseButtonHover();
});

function UserItemsHover() {
    // User items hovers
    $(".user-profile-window__item").hover(function() {
        $(this).css({
            "background-color": "#353540",
            "color": "white"
        });
    }, function() {
        $(this).css({
            "background-color": "transparent",
            "color": "#353540"
        });
    });
}

function BaseButtonHover() {
    // Add/Edit/Delete student button hover
    $(".general-button").hover(function() {
        $(this).css({
            "background-color": "darkgray",
            "transform": "translateY(-3px)"
        });
    }, function() {
        $(this).css({
            "background-color": "lightgray",
            "transform": "translateY(0px)"
        });
    });
}