<?php

header("Access-Control-Allow-Origin: *"); 
header("Access-Control-Allow-Methods: *");  
header("Access-Control-Allow-Headers: Content-Type");  
header("Content-Type: application/json");

if ($_SERVER["REQUEST_METHOD"] == "GET") {
    
    $group = $_GET["group"];
    $firstname = $_GET["firstname"];
    $lastname = $_GET["lastname"];
    $gender = $_GET["gender"];
    $birthday = $_GET["birthday"];
    $response = array();

    if (empty($firstname) || !preg_match('/^[A-Z][a-zA-Z]*$/', $firstname)) {
        $response["error"] = "Invalid firstname format";
        echo json_encode($response);
        exit();
    }

    if (empty($lastname) || !preg_match('/^[A-Z][a-zA-Z]*$/', $lastname)) {
        $response["error"] = "Invalid lastname format";
        echo json_encode($response);
        exit();
    }

    if (!empty($birthday)) {
        list($year, $month, $day) = explode('-', $birthday);
    }
    if (empty($birthday) || $year < 1964 || $year > 2007 || $day > 31 || $month > 12) {
        $response["error"] = "Invalid birthday format";
        echo json_encode($response);
        exit();
    }

    if (empty($response)) {
        $response["success"] = true;
        $response["group"] = $group;
        $response["firstname"] = $firstname;
        $response["lastname"] = $lastname;
        $response["gender"] = $gender;
        $response["birthday"] = $birthday;
    }

    echo json_encode($response);
} 

?>
